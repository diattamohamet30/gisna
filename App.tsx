import { StatusBar } from "expo-status-bar";
import Navigator from "./src/navigations";
import { NativeBaseProvider } from "native-base";

export default function App() {
  return (
    <NativeBaseProvider>
      <Navigator />
      <StatusBar />
    </NativeBaseProvider>
  );
}
