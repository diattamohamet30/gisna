import { View, Text } from "react-native";
import React from "react";
import { HStack } from "native-base";

interface CostumeHeaderProps {
  children: React.ReactNode;
}
export default function CostumeHeader({ children }: CostumeHeaderProps) {
  return (
    <HStack flex={1} space={300}>
      {children}
    </HStack>
  );
}
