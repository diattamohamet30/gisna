import { View } from "react-native";
import React from "react";
import styled from "styled-components/native";
import StyledRow from "../ui/Row";

const PotsItem = () => {
  return (
    <StyledCard>
      <Header>
        <StyledRow>
        <Text>Avatar</Text>
        <StyledColumn>
        <Text fontSize="20" >Sac a dos</Text>
        <Text>Dakar,Pikine</Text>
        </StyledColumn>
        </StyledRow>
        <StyledRow>
          <StyledButton>
          <Text>Icon</Text>
          </StyledButton>
        </StyledRow>
      </Header>
      <View style={{ flex: 2,display:"flex"}}>
        <Text>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Incidunt, fuga! </Text>
        <Text>il y a 2 jours <Text>par Mohamet</Text> </Text>
      </View>
      <View style={{
        display:"flex",
        flexDirection:"row"
      }}>
          <StyledButton>
          <Text color='black'>Avatar</Text>
          </StyledButton>
          <StyledButton>
          <Text color='black'>Avatar</Text>
          </StyledButton>
          <StyledButton>
          <Text color='black'>Avatar</Text>
          </StyledButton>
        </View>
    </StyledCard>
  );
};

export default PotsItem;

const StyledCard = styled.View`
  height: 220px;
  width: 360px;
  background-color: white;
  margin-top: 20px;
  border-radius: 20px;
  overflow: hidden;
  padding: 8px;
`;
const Header = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  padding: 18px;
`;

const StyledButton = styled.TouchableOpacity<{ color?: string }>`
  background-color: ${({ color }) => color || "#7e8282"};
  padding: 10px;
  border-radius: 5px;
  margin: 0px 0px 0px 8px;
`;
const Text = styled.Text<{ fontSize?: string; fontWeigth?: any,color?:string }>`
  color: ${({ color }) => color || "black"};
  font-weight: ${({ fontWeigth }) => fontWeigth || 400};
  font-size: ${({ fontSize }) => fontSize};
`;
const StyledColumn=styled.View`
display: flex;
flex-direction: column;
`
