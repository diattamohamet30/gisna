import { Container, Heading, Center } from "native-base";
import React from "react";
import { Text, View } from "react-native";
import { SafeAreaView } from "react-native-safe-area-context";
import styled from "styled-components/native";
import PotsItem from "../../components/PotsItem";

const Home = () => {
  return (
    <SafeAreaView>
      <Header>
        <LeftHeader>
          <Text>bb</Text>
          <Text>bb</Text>
        </LeftHeader>
        <RigthHeader>
          <Text>bb</Text>
        </RigthHeader>
      </Header>
      <Center>
        <Heading>Trouver Vos Objets Perdus</Heading>
        {/* <StyledInputContainer>
          <StyledInput />
        </StyledInputContainer> */}
        <PotsItem />
      </Center>
    </SafeAreaView>
  );
};

export default Home;

const Header = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  margin-left: 20px;
  margin-right: 20px;
  background-color: red;
  padding: 18px;
`;

const LeftHeader = styled.View`
  display: flex;
  flex: 1;
  justify-content: space-between;
  flex-direction: row;
  gap: 13px;
`;

const RigthHeader = styled.View`
  display: flex;
  flex: 3;
  justify-content: flex-end;
  flex-direction: row;
  gap: 10px;
`;
const StyledInput = styled.TextInput`
  padding: 20px;
  flex: 1;
  background-color: green;
  width: 100%;
  border-radius: 10px;
  text-align: center;
  color: red;
`;
const StyledInputContainer = styled.View`
  display: flex;
  flex-direction: row;
  flex: 1;
  margin-left: 20px;
  margin-right: 20px;
  margin-bottom: 40px;
`;
