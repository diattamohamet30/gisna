import { View, Text } from "react-native";
import React from "react";
import Home from "../screens/home";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer ,DefaultTheme} from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { HStack } from "native-base";

import Profile from "../screens/profile";
import Posts from "../screens/posts";
import Details from "../screens/details";
import CostumeHeader from "../components/CostumeHeader";
import { SafeAreaView } from "react-native-safe-area-context";

const Tabs = createBottomTabNavigator();
const Stack = createNativeStackNavigator();



const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: 'rgb(255, 45, 85)',
    backgroundColor:"gray"
  },
};

const TabsNavigator = () => (
  <Tabs.Navigator
    screenOptions={{
      headerShown: false,
    }}
  >
    <Tabs.Screen component={Home} name="Home" />
    <Tabs.Screen component={Profile} name="Profile" />
    <Tabs.Screen component={Posts} name="Posts" />
  </Tabs.Navigator>
);

export default function Main() {
  return (
    <NavigationContainer theme={MyTheme} >
      <Stack.Navigator 
      >
        <Stack.Screen
          component={TabsNavigator}
          name="Tabs"
          options={{ headerShown: false }}
        />
        <Stack.Screen component={Details} name="details" />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
