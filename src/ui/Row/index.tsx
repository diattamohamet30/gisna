import styled from "styled-components/native";

const StyledRow = styled.View`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
`;

export default StyledRow;
